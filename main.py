import glob
import os
import random
import pickle
import argparse
import tkinter as tk_i
import cv2
import requests
import json
import numpy as np

from tkinter import filedialog, Toplevel, messagebox
from PIL import Image, ImageTk, ImageDraw

class InspectionQuiz(object):

    def __init__(self, inspection_id, folder):
        self.inspection_id = inspection_id
        self.folder = folder
        self.image_list = [ f for f in os.listdir(folder) if f.endswith('.jpg') ]
        self.answer_dict = {}
        random.shuffle( self.image_list )

    def question(self):
        self.current_question = self.image_list.pop()
        return os.path.join(self.folder, self.current_question)

    def answer(self, choice):
        self.answer_dict[self.current_question] = choice

    def is_finish(self):
        return len(self.image_list) == 0

    def save(self):
        with open('{}.pickle'.format(self.inspection_id), 'wb') as f:
            pickle.dump(self.answer_dict, f, pickle.HIGHEST_PROTOCOL)

class InspectionIdWindow(tk_i.Toplevel):
    def __init__(self, master=None, callback=None):
        super().__init__(master)
        self.master = master
        self.inspection_id_entry = tk_i.Entry(self)
        self.inspection_id_entry.pack()
        self.ok_button = tk_i.Button(self, text='OK', width=10, command=callback)
        self.ok_button.pack()

class MainWindow(tk_i.Frame):

    def __init__(self, master=None, choose=['OK', 'NG'], ai_enable='False', ai_local='False'):
        super().__init__(master)
        self.master = master
        self.ai_enable = ai_enable
        self.ai_local = ai_local
        self.inspection_id_label = tk_i.Label(self, text="Inspection ID : xxxx")
        self.is_init_quiz = False
        image = Image.open("01.jpg")
        photo = ImageTk.PhotoImage(image)
        self.inspection_image = tk_i.Label(self, image=photo)
        self.inspection_image.image = photo
        self.choose_button = []
        for c in choose :
            b = tk_i.Button(self, text=c, width=10, command=lambda x=c: self.answer(x))
            self.choose_button.append( b )
        self.init_window()

    def init_window(self):
        self.master.geometry("1280x720")
        self.grid()
        menu = tk_i.Menu(self.master)
        self.master.config(menu=menu)
        file_menu = tk_i.Menu(menu, tearoff=False)
        file_menu.add_command(label="New Inspection Quiz", command=lambda : self.init_quiz())
        file_menu.add_command(label="Exit", command=self.master.quit)
        menu.add_cascade(label="File", menu=file_menu)
        self.inspection_id_label.grid(row=0, column=0)
        self.inspection_image.grid(row=1, column=0, rowspan=len(self.choose_button)+1)
        button_counter = 0
        for b in self.choose_button :
            b.grid(row=button_counter+1, column=1, padx=10, pady=8)
            button_counter += 1
        self.grid_rowconfigure(button_counter+1, weight=1)
        self.master.grid_columnconfigure(0, weight=1)
        self.master.grid_rowconfigure(0, weight=1)

    def init_quiz(self):
        self.directory = tk_i.filedialog.askdirectory()
        self.inspection_id_window = InspectionIdWindow(self, self.return_from_inspection_id_window)
        self.inspection_id_window.grab_set()

    def draw_softmax(self, image, softmax, cutoff=0.01):
        num_row = image.width // 128
        num_col = image.height // 128
        d = ImageDraw.Draw(image)
        counter = 0
        for each_softmax in softmax :
            x = (counter // num_col * 128) + 2
            y = (counter % num_col * 128) + 2
            if each_softmax[0] < cutoff :
                d.line( [(x, y), (x + 124, y), (x + 124, y + 124), (x, y + 124), (x, y)], fill='red', width=4)
            counter += 1
        del d

    def local_inference(self, sub_image, f_graph_file="coating_mobilenet_optimized_model.pb"):
        import tensorflow as tf
        with tf.gfile.GFile(f_graph_file, "rb") as f:
            restored_graph_def = tf.GraphDef()
            restored_graph_def.ParseFromString(f.read())
        with tf.Graph().as_default() as graph:
            cnn_input, softmax = tf.import_graph_def( restored_graph_def, input_map=None,
                         return_elements=['cnn_input:0', 'softmax:0'], name="")
        sess= tf.Session(graph=graph)
        feed_dict_testing = {cnn_input: sub_image}
        result=sess.run(softmax, feed_dict=feed_dict_testing)
        return result

    def init_question(self, image_file):
        image = Image.open(image_file)
        if self.ai_enable == 'True' :
            files = { 'inference_image': open(image_file, 'rb') }
            data = { 'inference_type' : '' }
            r = requests.post('https://35.184.157.215:8888/inspection/inference_v2',
                        files=files, data=data, verify=False)
            softmax = json.loads( r.text )['softmax']
            self.draw_softmax(image, softmax, cutoff=0.15)
        elif self.ai_local == 'True' :
            np_image = np.array(image)
            num_row = np_image.shape[1] // 128
            num_col = np_image.shape[0] // 128
            temp_softmax = np.zeros([num_row*num_col, 2])
            sub_image = []
            for row in range(num_row):
                for col in range(num_col):
                    start_x = row * 128
                    start_y = col * 128
                    sub_image.append( np_image[start_y:start_y+128, start_x:start_x+128, :] )
            m_softmax = self.local_inference(sub_image, f_graph_file="coating_mobilenet_optimized_model.pb")
            self.draw_softmax(image, m_softmax, cutoff=0.01)
        image = image.resize([image.width//2, image.height//2])
        photo = ImageTk.PhotoImage(image)
        self.inspection_image.config(image=photo)
        self.inspection_image.image = photo

    def answer(self, choice):
        if self.is_init_quiz :
            self.inspection_quiz.answer(choice)
            print("answer : " + choice)
            if self.inspection_quiz.is_finish():
                self.inspection_quiz.save()
                self.is_init_quiz = False
                tk_i.messagebox.showinfo("Done", "Thank you for join quiz.")
            else :
                self.init_question( self.inspection_quiz.question() )
        else :
            print("Quiz isn't init yet, please init quiz before start.")

    def return_from_inspection_id_window(self):
        self.inspection_id = self.inspection_id_window.inspection_id_entry.get()
        self.inspection_id_window.destroy()
        self.is_init_quiz = True
        self.inspection_id_label['text'] = 'Inspection ID : {}'.format(self.inspection_id)
        self.inspection_quiz = InspectionQuiz(self.inspection_id, self.directory)
        self.init_question( self.inspection_quiz.question() )

def main( *args ):
    root = tk_i.Tk()
    print(args[0])
    main = MainWindow(root, ai_enable=args[0].ai_enable, ai_local=args[0].ai_local)
    root.mainloop()

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='Inspection quiz for coating manufacturer.')
    parser.add_argument('--ai_local', nargs='?', default='False', const='True',
                choices=['True', 'False'], help='enable ai inference on local machine.')
    parser.add_argument('--ai_enable', nargs='?', default='False', const='True',
                choices=['True', 'False'], help='enable ai inference on server.')
    args = parser.parse_args()
    main( args )
